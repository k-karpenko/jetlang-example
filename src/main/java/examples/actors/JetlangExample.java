package examples.actors;

import org.jetlang.channels.Channel;
import org.jetlang.channels.MemoryChannel;
import org.jetlang.core.Callback;
import org.jetlang.core.Disposable;
import org.jetlang.fibers.Fiber;
import org.jetlang.fibers.PoolFiberFactory;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 12/10/12
 * Time: 2:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class JetlangExample {
    private static final Logger log = Logger.getLogger(JetlangExample.class.getCanonicalName());

    private static final float MIN_WEIGHT = 0.1f;
    private static final float MAX_WEIGHT = 1;
    private static final int ACTORS_COUNT = 4;
    private static final int MAX_ID = 10000;

    public static enum MessageStatus {
        CLEAN,
        FILTERED,
        ESTIMATED,
        CONSUMED
    }

    public static class Message {
        private String text;
        private MessageStatus status;
        private long publisherSign;
        private long consumerSign;
        private float random;
        private float weight;

        public Message(MessageStatus status, String text, long publisherSign ) {
            this.text = text;
            this.status = status;
            this.publisherSign = publisherSign;
            this.random = new Random().nextInt(MAX_ID);
        }
    }

    private static final Collection<Message> consumed = Collections.synchronizedList(new ArrayList<Message>());

    public static void main( String[] args ) {
        ExecutorService exec = Executors.newCachedThreadPool();

        PoolFiberFactory factory = new PoolFiberFactory(exec);

        final CountDownLatch latch = new CountDownLatch(ACTORS_COUNT);

        Disposable disposable = new Disposable() {
            @Override
            public void dispose() {
                latch.countDown();
            }
        };

        final Fiber publisher  = factory.create();
        publisher.add(disposable);
        final Fiber consumer = factory.create();
        consumer.add(disposable);
        final Fiber filter  = factory.create();
        filter.add(disposable);
        final Fiber estimator  = factory.create();
        estimator.add(disposable);

        final Channel<Message> queue = new MemoryChannel<Message>();
        queue.subscribe(publisher, new Callback<Message>() {
            @Override
            public void onMessage(Message message) {
                if ( !message.status.equals(MessageStatus.CONSUMED) ) {
                    return;
                }

                log.info("Adding processed message to a results list...");
                consumed.add( message );
                log.info("Added...");
                publisher.dispose();
            }
        });

        queue.subscribe(filter, new Callback<Message>() {
            @Override
            public void onMessage(Message message) {
                if ( !message.status.equals(MessageStatus.ESTIMATED) ) {
                    return;
                }

                log.info("Filtering...");
                if ( message.weight > MAX_WEIGHT || message.weight < MIN_WEIGHT ) {
                    log.info("This record would be filtered...");
                }

                message.status = MessageStatus.FILTERED;
                queue.publish(message);
                log.info("Filtered");
                filter.dispose();
            }
        });

        queue.subscribe(estimator, new Callback<Message>() {
            @Override
            public void onMessage(Message message) {
                if ( !message.status.equals(MessageStatus.CLEAN) ) {
                    return;
                }

                log.info("Estimation...");
                message.weight = ( message.random / MAX_ID );
                message.status = MessageStatus.ESTIMATED;
                queue.publish(message);
                log.info("Estimated");
                estimator.dispose();
            }
        });

        queue.subscribe(consumer, new Callback<Message>() {
            @Override
            public void onMessage(Message message) {
                if ( !message.status.equals(MessageStatus.FILTERED) ) {
                    return;
                }

                log.info("Consuming...");
                message.consumerSign = new Date().getTime();
                message.status = MessageStatus.CONSUMED;
                queue.publish(message);
                log.info("Consumed");
                consumer.dispose();
            }
        });

        publisher.start();
        consumer.start();
        filter.start();
        estimator.start();

        publisher.execute(new Runnable() {
            @Override
            public void run() {
                log.info("Publishing...");
                queue.publish( new Message(MessageStatus.CLEAN, "Message", new Date().getTime() ) );
                log.info("Published");
            }
        });

        try {
            latch.await();
        } catch ( InterruptedException e ) {
            e.printStackTrace();
        }

        log.info("Consumed: " + consumed.size() );
        exec.shutdown();
    }

}
